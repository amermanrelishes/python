import math, random

words = ['python', 'javascript', 'haskell', 'mathematica']
length = len(words)
# status = 0 means game not over
# status = 1 means user won
# status = 2 means user lost
status = 0
guessed_letters = ''
strikes = 0

print('Welcome to hangman! You will have 3 strikes to guess a word.\nHere is your word:')

pop_index = random.randint(0, length-1)
word = words.pop(pop_index)
original_word = word

hashed = ['#' for i in word]

while status == 0:
	hashed_word = ''
	for i in hashed:
		hashed_word += i

	# Check to see if user won
	if hashed_word.isalpha():
		status = 1
		print('Congrats! You won!!!')
		break

	print(hashed_word + ' ' + str(strikes) + ' strikes')

	while True:
		letter = input('guess a letter: ')
		if letter in guessed_letters:
			print('You already guessed that letter.')
		if len(letter) != 1:
			print('please guess a single letter.')
		elif not letter.isalpha():
			print('no symbols, please.')
		else:
			break

	while True:
		index = word.find(letter)
		if index == -1:
			guessed_letters += letter
			if letter not in original_word:
				strikes += 1
			break
		else:
			hashed[index] = letter
			word = word.replace(letter, '#', 1)

	if strikes >= 3:
		status = 2
		print('you lost! Booooo')
		break
